# nette-database

Database layer with a PDO-like API https://nette.org

[![PHPPackages Rank](http://phppackages.org/p/nette/database/badge/rank.svg)](http://phppackages.org/p/nette/database)
[![PHPPackages Referenced By](http://phppackages.org/p/nette/database/badge/referenced-by.svg)](http://phppackages.org/p/nette/database)

* https://www.tomasvotruba.cz/php-framework-trends/#nette